$title	Kappa Manufacturing Company

set	j	Products /A*C/,
	s	Shifts /day, night/,
	i	Operations /
		stamp	Stamping and welding, 
		sub	Subassembly, 
		final	Final assembly and packing/;

table	time(i,s)  Available time (hours)

			day	night
	stamp		30	35
	sub		25	30
	final		40	35;

table	a(i,j,s)	Productivity (units per hour)

	A.day	B.day	C.day	A.night	B.night	C.night
stamp	5	3	5	3	2	3	
sub	6	4	4	5	2	2
final	4	3	2	4	3	2;

parameter	p(j)	Selling prices /A 25, B 15, C 20/;

table	c(j,s)	Costs per unit by product and shift

		day	night
	A	11	16
	B	10	14
	C	12	18;

variables       PI      Profit,
                Y(j)    Aggregate production by product,
                X(j,s)  Production by product and shift;

nonnegative variables X, Y;

equations       profitdef, output, capacity;

profitdef..  PI =E= sum(j, P(j)*Y(j) - sum(s, c(j,s)*X(j,s)));

output(j)..      Y(j) =E= sum(s, X(j,s));

capacity(i,s)..  sum(j, X(j,s)/a(i,j,s)) =L= time(i,s);

Y.FX("A") = 60;
Y.LO("B") = 60;
Y.FX("C") = 40;

model kappa /all/;
solve kappa using LP maximizing PI;
