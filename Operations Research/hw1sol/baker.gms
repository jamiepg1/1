$title	Baker Paint Company

set	n /a*z/;

table	data(*,*)
	x	y	demand
a	2.16	2.44	10
b	2.32	2.43	2
c	3.45	2.08	5
d	4.25	2.47	2
e	4.37	2.83	1
f	4.09	2.87	4
g	5.09	2.59	3
h	3.21	2.88	6
i	2.25	3.13	7
j	2.03	4.13	4
k	2.54	3.71	2
l	3.27	3.37	1
m	4.35	3.46	9
n	5.12	3.27	4
o	4.84	4.05	5
p	3.81	3.83	3
q	2.99	4.19	4
r	2.47	4.6	1
s	3.07	4.47	5
t	4.14	4.53	2
u	3.77	4.7	6
v	2.63	4.81	1
w	3.45	5.19	10
x	4.65	4.45	2
y	4.85	4.63	8
z	4.53	5.22	10;


parameter       d(n)    Demand by node
                xl(n)   X location by node
                yl(n)   Y location by node;

d(n) = data(n,"demand");
xl(n) = data(n,"x");
yl(n) = data(n,"y");

variables       C       Total Shipping cost,
                X       X location of optimal location,
                Y       Y location of optimal location;

nonnegative 
variables       XD(n)   X dimension distance,
                YD(n)   Y dimension distance;

equations nXD, pXD, nYD, pYD, costdef;

nXD(n)..        XD(n) =g= X - xl(n);
pXD(n)..        XD(n) =g= xl(n) - X;
nYD(n)..        YD(n) =g= Y - yl(n);
pYD(n)..        YD(n) =g= yl(n) - Y;
costdef..       C =e= sum(n, d(n)*(XD(n)+YD(n)));

model grid /nXD, pXD, nYD, pYD, costdef/;

solve grid using LP minimizing C;


