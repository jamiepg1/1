$title	Southern Cotton Mill

set	j	Styles (products) /A*H/,
	i	Production processes /carding, drawing, spinning, weaving/;

parameter	p(j)	Contribution to profit ($ per 100000 yds) /
		A 400, B 370, C 360, D 360, E 350, F 330, G 280, H 275/;

table	a(j,i)	Unit Requirements (hours per 100000 yars) 

	carding	drawing	spinning weaving
A	1.8	36.0	720	 30.6
B	2.6	28.8	648	 30.6
C	2.4	28.8	720	 32.6
D	2.1	28.8	648	 30.6
E	2.9	31.2	648	 26.5
F	2.1	36.0	720	 30.6
G	2.9	21.6	792	 22.4
H	2.9	36.0	720	 32.8;

parameter	time(i)		Available time per 120 hour week (1000 hrs) /
	carding 26, drawing 242, spinning 7200, weaving 250/;
				 

variables       PI      Profit,
                Y(j)    Production by style (100000 yds);

nonnegative variable Y;

equations pidef, capacity;

pidef..         PI =E= sum(j, p(j)*Y(j));

capacity(i)..   sum(j, a(j,i)*Y(j)) =L= time(i);

model southern /pidef, capacity/;
solve southern using LP maximizing PI;
