$title	Lambda Fertilizer Company

set	i	Inputs /nitrates, phosphates, potash, inert/
	f	Fertilizer options /"5-10-5","5-10-10","10-10-10"/;

parameter  s(i)	Supplies 
		/nitrates 1000, phosphates 1800, potash 1200/;

table  a(f,i)	Input requirements

		nitrates	phosphates	potash
"5-10-5"	5		10		5
"5-10-10"	5		10		10
"10-10-10"	10		10		10;

a(f,"inert") = 100 - sum(i,a(f,i));
a(f,i) = a(f,i)/100;

parameters
	cost(i)	Costs of fertilizer ingredients /
		nitrates 160, phosphates 40, potash 100, inert 5/,
	price(f)  Market prices of fertilizers /
		"5-10-5"   40, "5-10-10"  50, "10-10-10" 60/;

variables       PI      Profit
                Y(f)    Production;

nonnegative variable Y;
equations profitdef, stock;

profitdef..  PI =e= 
       sum(f, Y(f)*(price(f)-15-sum(i, cost(i)*a(f,i))));

stock(i).. sum(f, Y(f)*a(f,i)) =l= s(i);

Y.LO("5-10-5") = 6000;

model lambda /all/;

s('inert') = +inf;

solve lambda using lp maximizing PI;
