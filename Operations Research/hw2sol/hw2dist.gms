$title	Illustrate Use of Distance-Matrix Interface

set	i	Origins /
	zur	"Zurich, Switzerland",
	gen	"Geneva, Switzerland",
	bas	"Basel, Switzerland"/,

	j	Destinations /
	lau	"Lausanne, Switzerland"
	ber	"Berne, Switzerland"
	luc	"Lucerne, Switzerland",
	stg	"St. Gallen, Switzerland",
	lug	"Lugano, Switzerland"/;

$batinclude distance-matrix i j
execute 'start dmatrix.html';
