$title  Thesis Management -- The Critical Path Method

set     a       Activities /
                A       Formulate an idea,
                B       Survey policy literature,
                C       Code up a small illustrative example,
                D       Write a note on the policy issues and context,
                E       Meet with advisor for idea approval
                F       Search academic literature
                G       Collect empirical data
                H       Formulate a more complete illustrative model
                I       Meet with advisor for model approval
                J       Formulate a calibrated pilot model
                K       Estimate key elasticities
                L       Meet with advisor for simulation approval
                M       Perform policy simulations
                N       Complete a thesis draft
                O       Obtain editorial feedback and advice
                P       Redraft the thesis
                Q       Meet with advisor for thesis acceptance /;

alias (i,j,a);

set     prec(i,j)       Precedence /
                A.(B,C,D),
                (B,C,D).E,
                E.(F,G,H),
                G.K,
                G.I,
                I.J,
                J.L,
                L.M,
                K.M,
                (F,M).N
                N.O,
                O.P,
                P.Q /;

parameter       pi(i,j) Probability of regression /
                E.A     0.25,
                L.I     0.25
                Q.P     0.15 /;

parameter       q(i)    Probability of completing project i;
q(i) = 1 - sum(j, pi(i,j));

parameter       delta   Time cost (days) /
                         A      15
                         B      4
                         C      5
                         D      10
                         E      4
                         F      15
                         G      15
                         H      5
                         I      5
                         J      10
                         K      4
                         L      3
                         M      4
                         N      20
                         O      5
                         P      2
                         Q      10/;


variable        T       Expected time requird to finish the thesis;

nonnegative
variables
                S(i)    Starting time,
                ED(i)   Expected delay  complete task i;

equations       start, delay, complete;

start(prec(i,j))..      S(j) =g= S(i) + ED(i);

delay(i)$(ED.LO(i)<ED.UP(I))..

                        ED(i) =e= q(i)*delta(i) + sum(j, pi(i,j)*(S(i)-S(j)+ED(i)));

complete(i)..           T =g= S(i) + ED(i);

model cpm    /start, delay, complete/;

*       First solve the problem with fixed duration (no regression);

ED.FX(i) = delta(i);
solve cpm using lp minimizing T;

*       Report which states are on the critital path:

set     critical(i)     Activities on the critical path;

critical(i) = yes$(ED.M(i) + sum(j, start.M(i,j)));
display critical;

*       Second, solve the problem with possibility of regression:

ED.LO(i) = 0;
ED.UP(i) = +INF;
solve cpm using lp minimizing T;

critical(i) = yes$(ED.M(i) + sum(j, start.M(i,j)) + complete.M(i));
display critical;



