set	i	Origins /
	zur	"Zurich, Switzerland",
	gen	"Geneva, Switzerland",
	bas	"Basel, Switzerland"/,

	j	Destinations /
	lau	"Lausanne, Switzerland"
	ber	"Berne, Switzerland"
	luc	"Lucerne, Switzerland",
	stg	"St. Gallen, Switzerland",
	lug	"Lugano, Switzerland"/;

parameter	c(i,j)	Travel Cost /
		zur.lau 235.822
		zur.ber 130.783
		zur.luc 53.988
		zur.stg 85.661
		zur.lug 206.589
		gen.lau 63.983
		gen.ber 168.013
		gen.luc 261.391
		gen.stg 354.654
		gen.lug 371.88
		bas.lau 200.904
		bas.ber 95.864
		bas.luc 97.092
		bas.stg 165.839
		bas.lug 263.25 /;

parameter	s(i)	Supply,
		d(j)	Demand;

s(i) = 100;
d(j) = 60;


Nonnegative
Variables	X(i,j)	Shipments;

Free
Variable	Z	Total cost;

equations	supply, demand, objdef;

supply(i)..	S(i) =g= sum(j, X(i,j));

demand(j)..	sum(i, X(i,j)) =g= D(j);

objdef..	Z =e= -sum((i,j), c(i,j)*X(i,j));

model mincost /all/;

solve mincost using lp maximizing Z;

s(i) = 300;

solve mincost using lp maximizing Z;
