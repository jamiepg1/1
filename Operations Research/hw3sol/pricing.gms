$title	Optimal Employment and Pricing


set	m	Months /JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC /,
	i	Employees /Joe, Ed, Harriet, Mabel/

alias (i,j);

table	workerdata(i,*)	Worker Characteristics

                  w           a       	    
Joe           0.341       0.270       	    
Ed            0.170       0.155       	    
Harriet       0.136       0.117       	    
Mabel         0.102       0.083;

parameter	w(i)	Wage rate
		a(i)	Productivity;

w(i) = workerdata(i,"w");
a(i) = workerdata(i,"a");


table	data	Monthly sales data

		d0		epsilon
	JAN	10		1
	FEB	10		1
	MAR	10		1
	APR	20		1
	MAY	30		1
	JUN	20		1
	JUL	20		1
	AUG	20		1
	SEP	40		1
	OCT	100		1
	NOV	120		0.5
	DEC	200		0.25;

parameter	d0(m)		Reference demand
		epsilon(m)	Elasticity of demand
		a(i)		Productivity;

d0(m) = data(m,"d0");
epsilon(m) = data(m,"epsilon");

variable	PROFIT	Maximand;

nonnegative
variables	P(m)	Price level
		D(m)	Demand
		C(m)	Cost
		H(i,m)	Hours;

equations profitdef, demand, cost, output;

demand(m)..	D(m) =e= d0(m) * (1 - epsilon(m)*(P(m)-1));

cost(m)..	C(m) =e= sum(i, w(i)*H(i,m));

output(m)..	D(m) =L= sum(i, a(i) * H(i,m));

profitdef..	profit =e= sum(m, P(m)*D(m) - C(m));

model profitmax /all/;

H.UP(i,m) = 100;

solve profitmax using nlp maximizing profit;

equation	employ  Bound on employment change;

employ(i,m++1)..  H(i,m++1) =g= 0.8*H(i,m);

model hbound /all/;

solve hbound using nlp maximizing profit;

equation	pricing		Limits on price variation;

pricing(m++1)..	P(m++1) =L= 1.4*P(m);

model pbound /all/;

solve pbound using nlp maximizing profit;

