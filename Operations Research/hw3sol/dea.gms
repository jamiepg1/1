$title  Data Envelope Analysis


set     data /
        Y       Output
        L1      Production workers
        L2      Non-production workers
        L       Total labor
        K       Fixed capital
        F       Fuel
        M       Materials /


set     s       States /
        JK      Jammu & Kashmir
        HP      Himachal Pradesh
        PU      Punjub
        CH      Chandigarh
        UT      Uttaranchal
        HA      Haryana
        DE      Delhi
        RA      Rajasthan
        UP      Uttar Pradesh
        BI      Bihar
        AS      Assam
        WB      West Bengal
        JH      Jharkhand
        OR      Orissa
        CT      Chhattisgarh
        MP      Madhya Pradesh
        GU      Gujarat
        MH      Maharashthra
        AP      Andhra Pradesh
        KA      Karnataka
        GO      Goa
        KE      Kerala
        TN      Tamilnadu /;

table stats(s,data)  India ASI input-output data in manufacturing 2002-03

*       Labor inputs are measured by number of persons employed;
*       all other inputs are in 1000 lakhs of Rupees at current prices
*       (1 lakh=0.1 million).

             Y          L1           L           K           F           M
JK     175.631      19.636      24.881      37.863       8.361     108.479
HP     611.899      25.375      34.023     358.278      52.533     323.166
PU    4057.079     276.677     351.102    1119.761     322.346    2614.087
CH     114.081       5.180       8.243      28.005       5.072      61.798
UT     603.559      27.815      41.485     204.586      49.348     342.505
HA    5261.740     223.831     299.765    1410.886     209.354    3381.965
DE    1724.437      85.693     127.935     249.193      44.935     979.492
RA    3311.696     190.971     244.265    1355.028     362.936    1868.534
UP    8052.063     409.116     542.160    2847.631     475.443    4930.123
BI     807.680      44.280      54.184     317.048      23.045     551.546
AS    1250.610      93.129     110.879     596.413      64.932     766.956
WB    4873.244     428.096     538.858    2458.806     320.238    3041.783
JH    2476.429     121.427     156.497    1576.865     264.896    1082.276
OR    1486.235      92.686     118.187    1061.308     210.580     747.136
CT    1449.707      63.771      93.794     728.551     209.379     669.932
MP    3805.750     156.565     208.874    1389.786     304.492    2228.977
GU   18269.979     528.217     717.055    8235.801     981.847   12488.798
MH   21759.551     829.305    1170.461    7697.089     970.795   13006.687
AP    8228.642     864.822    1007.463    3206.186     432.844    5466.125
KA    6568.082     370.217     485.917    3307.273     349.655    3664.688
GO     895.740      24.318      35.061     342.420      43.042     544.433
KE    2665.085     227.347     270.548     679.483     132.151    1885.033
TN   10807.543     920.127    1125.497    4347.893     708.413    6697.103;

stats(s,"L2") = stats(s,"L") - stats(s,"L1");
display stats;


set     m(data) Metrics for assessing efficiency /
        L1      Production workers
        L2      Non-production labor
        K       Fixed capital
        F       Fuel
        M       Materials /;

parameter
        delta(s)        Weighting vector for optimal choice of weights
        score(s,m)      Scoring of raw data;

score(s,m) = stats(s,"Y")/stats(s,m);

*       1. Primal Model

nonnegative
variables       EFF(s)  Efficiency of state s,
                WT(m)   Weight by metric;

free variable   OBJ     Objective function;

equations objdef, effbound;

objdef..        OBJ =e= sum(s, delta(s)*EFF(s));

effbound(s)..   EFF(s) =e= sum(m, WT(m)*score(s,m));

model dea /all/;

EFF.UP(s) = 1;

*       2. Dual Model

variables       MU(s)           Multiplier state s upper bound
                LAMDA(s)        Multiplier state s efficiency;

nonnegative variable MU;

equations       objdual, dualeff, dualwt;

objdual..       OBJ =e= sum(s, MU(s));

dualEFF(s)..    delta(s) =l= LAMDA(s) + MU(s);

dualWT(m)..     sum(s, LAMDA(s)*score(s,m)) =l= 0;

model deadual /objdual, dualeff, dualwt/;

parameter       report          Summary report of efficiency weights;

alias (s,ss);
loop(ss,
        delta(s) = 1$sameas(ss,s);

        solve DEA using LP maximizing OBJ;
        report(ss,m) = WT.L(m);
        report(ss,"Score") = EFF.L(ss);


);

display report;

display OBJ.L;
