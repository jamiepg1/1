set
b Beer types /Light, Dark/,
i Ingredients /Malt, Hops, Yeast/;

table data(*,*) Model data
         Malt    Hops    Yeast   price
Light    2       3       2       2000
Dark     3       1       1.66    1000
supply   75      60      50              ;

parameters
p(b)    Market price,
a(i,b)  Input requirements matrix,
s(i)    Resource supply;

p(b) = data(b,"price");
s(i) = data("supply",i);
a(i,b) = data(b,i);

Positive variable q(b) quantities of beer to produce;
Variable profit Total profit earned from selling beers;

Equations Qprofit, Qingred(i);

Qprofit.. profit =e= sum(b,p(b)*q(b));
Qingred(i).. sum(b,q(b)*a(i,b)) =l= s(i);

Model beer /all/;
Solve beer using lp maximizing profit;








