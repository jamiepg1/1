$title Rock Concert Planning

set activity /
         A "Find Site",
         B "Find Engineers",
         C "Hire Opening Act",
         D "Set Radio and TV Ads",
         E "Set Up Ticket Agents",
         F "Prepare Electronics",
         G "Print Advertising",
         H "Set up Transportation",
         I "Rehearsals",
         J "Last-Minute Details"/;

parameter duration(activity) "in days" /
         A 3,
         B 2,
         C 6,
         D 2,
         E 3,
         F 3,
         G 5,
         H 1,
         I 1.5,
         J 2 /;

alias(activity,ai,aj);

set prec(ai,aj) "Precedence order" /
         A.(B,C,E)
         B.F
         C.(D,G,H)
         (F,H).I
         I.J /;

Variables
when(activity) When does the activity occurs,
totTime;

Positive variable time total time of concert preparation;

equations incidence(ai,aj), endTime(activity);
incidence(ai,aj)$prec(ai,aj).. time(aj) =g= time(ai) + duration(ai);
endTime(activity).. totTime =g= time(activity) + duration(activity);

Model RockConcertTime /all/;
Solve RockConcertTime using lp minimizing totTime;
